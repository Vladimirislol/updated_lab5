public class Students {
	public String name;
	public String course;
	public int grade;
	public int amountLearnt;
	public int amountStudied;
	
	public Students(String name, int grade) {
		this.name = name;
		this.course = "UNASSIGNED";
		this.grade = grade;
		this.amountLearnt = 0;
		this.amountStudied = 0;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getCourse() {
		return this.course;
	}
	public void setCourse(String newCourse) {
		this.course = newCourse;
	}
	
	public int getGrade() {
		return this.grade;
	}
	
	public void learn(int amountStudied) {
		if(amountStudied > 0) {
		this.amountLearnt += amountStudied;
		}
	}
	public void myGrade(int grade) {
		System.out.println("my grade is "+grade+"!");
		}
	public void info(String name, String course) {
		System.out.println("My name is "+name+" and I am in the course "+course+"!");
	}
}